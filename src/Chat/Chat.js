import React from "react";
import MessageList from "./MessageList";
import Preloader from "./Preloader";
import MessageInput from "./MessageInput";
import Header from "./Header";
import ModalWindow from "./ModalWindow";
import { v4 as uuidv4 } from "uuid";

import { connect } from "react-redux";
import {
  loadMessages,
  turnOffLoad,
  updateOnlineDate,
  updateUsersInChat,
  updateMessagesCountInChat,
  editMessage,
  showModalWindow,
  tempId,
} from "../actions/";

class Chat extends React.Component {
  constructor() {
    super();
    this.addMessage = this.addMessage.bind(this);
    this.editMessage = this.editMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.arrowUpHandler = this.arrowUpHandler.bind(this);
  }

  renderTime(createdAt) {
    const date = new Date(createdAt);

    return (
      date.getDate().toString().padStart(2, "0") +
      "." +
      (date.getMonth() + 1).toString().padStart(2, "0") +
      "." +
      date.getFullYear().toString().padStart(4, "0") +
      " " +
      date.getHours().toString().padStart(2, "0") +
      ":" +
      date.getMinutes().toString().padStart(2, "0")
    );
  }

  usersCount(array) {
    let isThere = [];
    let numOfUsers = 0;
    array.forEach((val) => {
      if (!isThere.includes(val.user)) {
        isThere.push(val.user);
        numOfUsers++;
      }
    });
    return numOfUsers;
  }

  async componentDidMount() {
    const url = this.props.url;
    const res = await fetch(url);
    const messagesData = await res.json();

    if (messagesData.length === 0) {
      return 0;
    }

    const usersInChatCount = this.usersCount(messagesData);

    let TodayDate;
    if (messagesData[messagesData.length - 1].createdAt) {
      TodayDate = this.renderTime(
        messagesData[messagesData.length - 1].createdAt
      );
    } else {
      TodayDate = this.renderTime(new Date());
    }

    this.props.turnOffLoad(messagesData);
    this.props.loadMessages(messagesData);
    this.props.updateMessagesCountInChat(messagesData.length);
    this.props.updateUsersInChat(usersInChatCount);
    this.props.updateOnlineDate(TodayDate);

    window.addEventListener("keydown", (event) => {
      if (event.keyCode === 38) {
        this.arrowUpHandler();
      }
    });
  }

  arrowUpHandler() {
    const currentMessages = [...this.props.reducer.messages];

    let isOwn = currentMessages.some((element) => {
      return element.fromSender === true;
    });

    if (!isOwn) {
      return false;
    }

    let lastMessageId;
    const lengthMessages = currentMessages.length - 1;
    for (let i = lengthMessages; i >= 0; i--) {
      if (currentMessages[i].fromSender === true) {
        lastMessageId = currentMessages[i].id;
        break;
      }
    }

    this.props.showModalWindow(true);
    this.props.tempId(lastMessageId);
  }

  addMessage(myText, clear) {
    const OldMessage = [...this.props.reducer.messages];
    let OldMessageCount = this.props.reducer.messageNumber;
    OldMessageCount++;
    const currentDate = new Date();
    const newMessage = {
      user: "Admin",
      createdAt: currentDate,
      id: uuidv4(),
      fromSender: true,
      text: myText,
    };

    OldMessage.push(newMessage);

    this.props.loadMessages(OldMessage);
    this.props.updateOnlineDate(this.renderTime(currentDate));
    this.props.updateMessagesCountInChat(OldMessageCount);

    const usersInChatCount = this.usersCount(OldMessage);
    this.props.updateUsersInChat(usersInChatCount);

    clear();
  }

  editMessage(newText, id, date) {
    const OldMessage = [...this.props.reducer.messages];
    let indexForEdited = OldMessage.findIndex((message) => {
      return message.id === id;
    });
    OldMessage[indexForEdited].text = newText;
    OldMessage[indexForEdited].createdAt = date;
    this.props.loadMessages(OldMessage);
  }

  deleteMessage(id) {
    let OldMessage = [...this.props.reducer.messages];
    let OldMessageCount = this.props.reducer.messageNumber;
    OldMessageCount--;
    OldMessage = OldMessage.filter((val) => {
      if (val.id !== id) {
        return val;
      }
    });

    const usersInChatCount = this.usersCount(OldMessage);
    this.props.loadMessages(OldMessage);
    this.props.updateMessagesCountInChat(OldMessageCount);
    this.props.updateUsersInChat(usersInChatCount);
  }

  render() {
    const {
      editModal,
      preloader,
      messages,
      messageNumber,
      usersInChat,
      lastMessageSent,
    } = this.props.reducer;
    if (!preloader) {
      return (
        <div className="chat">
          <Header
            messages={messageNumber}
            userInChat={usersInChat}
            lastMessage={lastMessageSent}
          />
          <MessageList
            messages={messages}
            edit={this.editMessage}
            delete={this.deleteMessage}
          />
          <MessageInput renderMessage={this.addMessage} />
          {editModal ? <ModalWindow edit={this.editMessage} /> : ""}
        </div>
      );
    } else {
      return <Preloader />;
    }
  }
}

const mapStateToProps = (state) => {
  return { reducer: state.chat };
};

const mapDispatchToProps = () => {
  return {
    loadMessages,
    turnOffLoad,
    updateOnlineDate,
    updateUsersInChat,
    updateMessagesCountInChat,
    editMessage,
    showModalWindow,
    tempId,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Chat);
