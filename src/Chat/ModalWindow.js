import React from "react";
import "./ModalWindow.css";

import { connect } from "react-redux";
import { showModalWindow, tempText, tempId } from "../actions/";

class ModalWindow extends React.Component {
  constructor() {
    super();
    this.readText = this.readText.bind(this);
    this.sendChanges = this.sendChanges.bind(this);
    this.state.text = "";
  }

  state = { text: "" };

  sendChanges() {
    if (this.state.text === undefined || this.state.text === "") {
      this.props.showModalWindow(false);
    } else {
      this.props.tempText(this.state.text);
      this.props.showModalWindow(false);

      const dateEdit = new Date();
      this.props.edit(
        this.state.text,
        this.props.reducer.tempIdValue,
        dateEdit
      );
    }
  }

  readText(e) {
    this.props.reducer.tempEditedText = e.target.value;
    this.setState({
      text: this.props.reducer.tempEditedText,
    });
  }

  render() {
    return (
      <div
        className={
          this.props.reducer.editModal
            ? "edit-message-modal modal-shown"
            : "edit-message-modal"
        }
      >
        <div className="block-modal">
          <h1>Enter new text:</h1>
          <div className="line"></div>
          <input
            type="text"
            className="edit-message-input"
            onChange={this.readText}
            value={this.props.reducer.tempEditedText}
          />
          <div className="buttonsField">
            <button
              className="buttonStyle edit-message-close"
              onClick={() => this.props.showModalWindow(false)}
            >
              Cancel
            </button>
            <button
              className="buttonStyle edit-message-button"
              onClick={() => this.sendChanges()}
            >
              Edit
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { reducer: state.chat };
};

const mapDispatchToProps = () => {
  return {
    showModalWindow,
    tempText,
    tempId,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(ModalWindow);
